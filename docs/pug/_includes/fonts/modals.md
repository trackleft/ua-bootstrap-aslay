<div id="specimen-modal" tabindex="-1" class="modal fade bs-example-modal-lg">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button data-dismiss="modal" type="button" class="btn pull-right"><span class="ua-brand-x"></span></button>
          <h4 id="myModalLabel" class="modal-title">FONT SAMPLE</h4>
        </div>
        <div class="modal-body">
          <h1 id="js-change-font-name">FF Milo Serif Web Black Italic</h1>
          <div class="sample shadow">
            <form id="bigcontrol" action="javascript:customView Sample();" name="bigcontrol"><a id="abcView" name="abcView SampleBtn" onclick="abcSample()" type="button" style="margin-right:15px; margin-bottom:15px;" class="SampleBtn btn btn-mesa">ABC</a>
              <p><strong>Or try your own text:</strong></p><span class="nobr">
                <div class="input-group">
                  <input maxlength="120" name="customSampleText" size="62" type="text" value="Bear Down, Arizona. Bear Down, red and blue." class="form-control"/>
                  <div class="input-group-btn"><a id="customSampleBtn" name="customSampleBtn" onclick="customSample()" type="button" class="btn btn-red">Go</a></div>
                </div></span>
            </form>
            <table id="js-specimen-modal-font" class="table table-striped">
              <tbody>
                <tr>
                  <td>
                    <p id="big1" style="font-size:72px" class="sample-text">All hail, Arizona! Thy colors Red and Blue Stand as a symbol of our love for you.</p>
                  </td>
                  <th>
                    <p>72px</p>
                  </th>
                </tr>
                <tr>
                  <td>
                    <p id="big2" style="font-size:48px" class="sample-text">All hail, Arizona! Thy colors Red and Blue Stand as a symbol of our love for you.</p>
                  </td>
                  <th>
                    <p>48px</p>
                  </th>
                </tr>
                <tr>
                  <td>
                    <p id="big3" style="font-size:36px" class="sample-text">All hail, Arizona! Thy colors Red and Blue Stand as a symbol of our love for you.</p>
                  </td>
                  <th>
                    <p>36px</p>
                  </th>
                </tr>
                <tr>
                  <td>
                    <p id="big4" style="font-size:28px" class="sample-text">All hail, Arizona! Thy colors Red and Blue Stand as a symbol of our love for you.</p>
                  </td>
                  <th>
                    <p>28px</p>
                  </th>
                </tr>
                <tr>
                  <td>
                    <p id="big5" style="font-size:24px" class="sample-text">All hail, Arizona! Thy colors Red and Blue Stand as a symbol of our love for you.</p>
                  </td>
                  <th>
                    <p>24px</p>
                  </th>
                </tr>
                <tr>
                  <td>
                    <p id="big6" style="font-size:20px" class="sample-text">All hail, Arizona! Thy colors Red and Blue Stand as a symbol of our love for you.</p>
                  </td>
                  <th>
                    <p>20px</p>
                  </th>
                </tr>
                <tr>
                  <td>
                    <p id="big7" style="font-size:16px" class="sample-text">All hail, Arizona! Thy colors Red and Blue Stand as a symbol of our love for you.</p>
                  </td>
                  <th>
                    <p>16px</p>
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
