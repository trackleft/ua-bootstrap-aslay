  <h2 id="external-links">External Links</h2>
  <p>For better user experience, you can add an external-link icon by using `.ext`. </p>
  <p>You can add `.ext-blue` for a blue version. </p>
  <div class="example" data-example-id="large-well">
    <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
    <p><a href="#" class="ext">External link with the default icon.</a></p>
    <p><a href="#" class="ext ext-blue">External link with an alternate blue icon.</a></p>
  </div>
  ```html
  <a href="#" class="ext">External link with the default icon.</a>
  <a href="#" class="ext ext-blue">External link with an alternate blue icon.</a>
  ```
  